# Dépôt Adullact-Faq-Theme

## Objet

Ce dépôt a pour objet d'héberger les modifications faites par l'ADULLACT sur
le thème Hugo [Geekdoc](https://github.com/thegeeklab/hugo-geekdoc).

Les éventuelles contributions à faire *upstream* en seront facilitées.

## Configuration sur un poste client

Ajouter le dépôt upstream :

```shell script
git remote add upstream https://github.com/thegeeklab/hugo-geekdoc.git
```

## Spécificités de ce dépôt

**Attention** La branche *par défaut* est `geekdoc-adullact`. Ceci permet :

* de créer des *merges requests* directement sur notre branche, sans altérer la
  branche master/main du thème
* de maintenir une correspondance propre entre la branche `master` de notre dépôt
  et la branche `main` côté *upstream*
* de faciliter les diffs et donc d'éventuelles contributions *upstream*
